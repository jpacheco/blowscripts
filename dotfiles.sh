#!/bin/sh

dotfiles () {
    doas -u javier git clone https://git.disroot.org/jpacheco/voidots $HOME/.dotfiles --depth 1 && \
        doas -u javier ln -sf .dotfiles/home/.config $HOME/.config
        doas -u javier ln -sf .dotfiles/home/.local $HOME/.local
        doas -u javier ln -sf .config/shell/profile .$HOME/zprofile
        doas -u javier ln -sf .config/x11/xprofile $HOME/.xprofile
}

dotfiles && echo "symlinks of dotfiles are now created" || echo "Check your internet conection!!!"

